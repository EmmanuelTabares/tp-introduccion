**El ejercicio del poder**

 

Para derrocar el Poder político
es siempre necesario, ante todo crear opinión pública y trabajar en el terreno
ideológico.   

Así proceden las clases revolucionarias, y también las clases
contrarrevolucionarias.

Mao

** **

**Introducción**

                El
trabajo que presentamos a continuación está concebido como un aporte al
análisis del ejercicio del poder en el mundo contemporáneo.

                Partiendo
de tres emergentes culturales (el film “Get Out!”, nominada al Oscar como mejor
película en su 90° edición de 2018; el documental “La muerte y vida de Martha
P. Johnson”, nominada como mejor en su categoría en el Festival de Tribeca para
la 16° edición del año 2017; y el artículo de opinión de Jaime Duran Barba,
“Libertad y comunicación política”, publicado el 22 de abril de 2018 en la
edición dominical del periódico Perfil de la Argentina), emplearemos para ello
como instrumento de análisis la bibliografía de la cátedra Taller de
“Introducción a la problemática del mundo contemporáneo” de la Universidad
Nacional de Tres de Febrero para su ciclo lectivo de 2018.

                Al
finalizar, nuestras conclusiones abordaran de manera polémica la
caracterización compartida por varios autores de relevancia académica quienes a
partir de una demarcación histórica que ubica entre las décadas del ’60 y ’70
del siglo pasado un momento de escisión sostienen que se habría dado a luz una
posible nueva época.

 

1.-Fundamentos ideológicos de la
dominación

(Acá irían todas los
ejemplos que encontremos en los “emergentes” para seguir lo de De Souza acerca
de las “premisas de la dominación”: racismo, desarrollo, superior-inferior.
Creo recordar que también en Bauman hay planteos sobre las justificaciones que
encuentran los poderosos para su accionar. Relación con “los tres”: la
argumentación del “hermano/soldado” en Get out! Que ya había destacado Alejo;
la constante despersonificación de los trans en La muerte…)

 

2.-Impregnación de la cultura
popular (o popularizada)

(Acá tenemos las
explicaciones previas que hace De Souza para explicar descolonización cultural.
Se podría utilizar parte de la bibliografía de la unidad de medios de
comunicación y sociedad de masas. En los tres: vincular con la acusación que Silvia
Rivera hace a su mismo movimiento de considerar a los trans un estereotipo,
también planteos sobre el apoyo a Obama por parte del padre de familia en Get
Out!)

 

3.- El rol del control y el
castigo

(Como castigo tenemos
claramente lo que aparece en el documental, lo de control tiendo a pensarlo
menos como modelo-espectador que como seducción-absorción del discurso, que es
lo que aparece en Duran Barba, pero habría que seguir buscando. Acá
principalmente destacar lo de Gilles Deleuze, que el control y el castigo
conviven, no se reemplazan. Un ejemplo sería genial. También tenemos lo de la
indescifrabilidad da poder, de Bauman, creo.)

4.-Absorción de los discursos de
oposición

(Acá no sólo está lo más
visible de Duran Barba, sino también lo del desfile del orgullo gay copado por
la mafia, y todas las formas de aparecer contemplativos frente al reclamo pero
cagarlos. En esto hay que rastrear más lo de los autores porque en general se
les pasa de largo. También podemos hacer referencia a que no lo vieron, por
ejemplo De Souza y su fe en los movimientos sociales, lo mismo que Bauman. El
que más los critica parece Bruera a los más ridículos
ambientalistas-vegetarianos.)

 

**Conclusiones**

(Como dijimos, esto hay que
hablarlo. La idea central es que si el ejercicio del poder ha adquirido nuevas
maneras para no ser desplazados y para controlar la falsa nueva libertad
(Byung-Chul Han) que sólo tiene por objetivo dar a las clases intermedias
globales (entre los poderosos de Davos y los locales) capacitación en
tecnología que aumente la productividad económica, ¿es posible hablar de cambio
de época? ¿O es una forma de auto engaño (para ser leves, para no hablar de lo
que quieren inculcarnos a nosotros) de los intelectuales hasta ahora estudiados
para no desanimarse (a lo De Souza) o quedar en falsa escuadra hasta que
aparezca la próxima potencia que enfrente a los yanquis?) 